<?php

/**
 * +----------------------------------------------------------------------
 * | 润憬商城系统 [ 高性价比的通用商城系统 ]
 * +----------------------------------------------------------------------
 * | Copyright (c) 2022~2023 https: *www.honc.fun All rights reserved.
 * +----------------------------------------------------------------------
 * | Licensed 这不是一个自由软件，不允许对程序代码以任何形式任何目的的再发行
 * +----------------------------------------------------------------------
 * | Author: 润憬科技 Hon(陈烁临) <2275604210@qq.com>
 * +----------------------------------------------------------------------
 */

namespace app\middleware;

use Webman\MiddlewareInterface;
use Webman\Http\Response;
use Webman\Http\Request;

class AccessControl implements MiddlewareInterface
{
  public function process(Request $request, callable $handler): Response
  {
    // 如果是opitons请求则返回一个空的响应，否则继续向洋葱芯穿越，并得到一个响应
    $response = $request->method() == 'OPTIONS' ? response('') : $handler($request);

    // 给响应添加跨域相关的http头
    $response->withHeaders([
      'Access-Control-Allow-Credentials' => 'true',
      'Access-Control-Allow-Origin' => $request->header('origin', '*'),
      'Access-Control-Allow-Methods' => $request->header('access-control-request-method', '*'),
      'Access-Control-Allow-Headers' => $request->header('access-control-request-headers', '*'),
    ]);

    return $response;
  }
}
