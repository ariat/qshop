<?php

namespace app\controller;

use support\Request;

class Index
{
    public function index(Request $request)
    {
        return response('hello webman');
    }

    public function view(Request $request)
    {
        return view('install/step_0', ['cfg_copyright' => 'webman']);
        return view('index/view', ['name' => 'webman']);
    }

    public function json(Request $request)
    {
        return json(['code' => 0, 'msg' => 'ok']);
    }

    public function install()
    {
        return 'install';
    }
}
