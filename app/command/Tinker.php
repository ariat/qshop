<?php

namespace app\command;

use Psy\Configuration;
use Psy\Shell;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Output\OutputInterface;



class Tinker extends Command
{
    protected static $defaultName = 'tinker';
    protected static $defaultDescription = 'Interact with your application';


    /**
     * @return void
     */
    protected function configure()
    {
        $this->addArgument('name', InputArgument::OPTIONAL, 'Name description');
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->getApplication()->setCatchExceptions(false);

        $config = new Configuration([
            'updateCheck' => 'never',
        ]);

        $shell = new Shell($config);
        // $shell->setIncludes();

        // if (isset($_ENV['COMPOSER_VENDOR_DIR'])) {
        //     $path = $_ENV['COMPOSER_VENDOR_DIR'];
        // } else {
        //     $path = base_path() . DIRECTORY_SEPARATOR . 'vendor';
        // }

        // $path .= '/composer/autoload_classmap.php';

        try {
            $shell->run();
        } finally {
        }
    }
}
